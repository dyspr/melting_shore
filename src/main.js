var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var shifting
var ratio
var numOfStrips = 13
var strips = []

function preload() {
  shifting = loadImage('img/fortepan_193370.jpg')
}

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)

  ratio = (shifting.width / boardSize) * 0.75

  for (var i = 0; i < numOfStrips; i++) {
    strips.push(shifting.get(0, 0 + i * (shifting.height / numOfStrips), shifting.width, (shifting.height / numOfStrips)))
  }
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)
  imageMode(CENTER)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < strips.length; i++) {
    push()
    translate(windowWidth * 0.5, windowHeight * 0.5)
    rotate(Math.PI * 0.15)
    translate(sin(frameCount * 0.01 + i * 0.2) * boardSize * 0.1, (i - Math.floor(strips.length * 0.5)) * (shifting.height / numOfStrips) * ratio)
    image(strips[i], 0, 0, strips[i].width * ratio + cos(frameCount * 0.05 + i * 0.25) * boardSize * 0.025, strips[i].height * ratio)
    pop()
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)

  ratio = (shifting.width / boardSize) * 0.75
}
